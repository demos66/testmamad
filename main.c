#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// CWE-119
void copyData(const char* input) {
    char buffer[10];
    strcpy(buffer, input); // Vulnerable line - no bounds checking on input size
    printf("Buffer contents: %s\n", buffer);
}

// CWE-125
void vulnerable_function(char *input_string) {
    char buffer[10];
    strcpy(buffer, input_string); // potential out-of-bounds read vulnerability
    printf("%s\n", buffer);
}

// CWE-20
void display_user(char *username) {
    char sql_query[1000];
    sprintf(sql_query, "SELECT * FROM users WHERE username='%s'", username);
    // Execute the SQL query and display the result
}

// CWE-22
void read_file(char* filename) {
  char path[100];
  snprintf(path, sizeof(path), "/home/user/data/%s", filename);
  FILE* file = fopen(path, "r");
  if (file) {
    printf("File contents:\n");
    char buffer[1024];
    while (fgets(buffer, sizeof(buffer), file)) {
      printf("%s", buffer);
    }
    fclose(file);
  } else {
    printf("File not found.\n");
  }
}

// cwe-476
void vulnerable_function2(char* input) {
  char buffer[100];
  strcpy(buffer, input);

  printf("Copying %d bytes to buffer...\n", strlen(input));

  if (strlen(input) > 50) {
    printf("Input too long!\n");
    return;
  }

  char command[100];
  sprintf(command, "echo %s", buffer);
  system(command);

  char* ptr = NULL;
  *ptr = 'a'; // dereferencing a NULL pointer

  int i;
  for (i = 0; i < strlen(buffer); i++) {
    buffer[i] += 10; // data corruption
  }

  printf("Modified buffer: %s\n", buffer);
}

// cwe-798
void authenticate(char* username, char* password) {
    char validUsername[] = "admin";
    char validPassword[] = "password123";

    if (strcmp(username, validUsername) == 0 && strcmp(password, validPassword) == 0) {
        printf("Authentication successful.\n");
    } else {
        printf("Authentication failed.\n");
    }
}

// cwe-476
void vulnerable_function3(char* input) {
  char buffer[100];
  strcpy(buffer, input);

  printf("Copying %d bytes to buffer...\n", strlen(input));

  if (strlen(input) > 50) {
    printf("Input too long!\n");
    return;
  }

  char command[100];
  sprintf(command, "echo %s", buffer);
  system(command);

  char* ptr = NULL;
  *ptr = 'a'; // dereferencing a NULL pointer

  int i;
  for (i = 0; i < strlen(buffer); i++) {
    buffer[i] += 10; // data corruption
  }

  printf("Modified buffer: %s\n", buffer);
}


int main(int argc, char* argv[]) {
    // CWE-119
    char userInput[20];
    printf("Enter your input: ");
    scanf("%s", userInput);
    copyData(userInput);

    // CWE-125
    char input[20] = "This is a test";
    vulnerable_function(input);

    // CWE-190
    int arr[10];
    int index = 0;
    int input2 = 0;

    // read integers from user input and store them in the array
    while (scanf("%d", &input2) == 1) {
        arr[index] = input2;
        index++;
    }

    // process the array
    int sum = 0;
    for (int i = 0; i <= index; i++) {
        sum += arr[i];
    }

    printf("The sum of the numbers is: %d\n", sum);

    // CWE-20
    char username[100];
    printf("Enter username: ");
    scanf("%s", username);
    display_user(username);

    // CWE-22
    if (argc < 2) {
    printf("Usage: %s filename\n", argv[0]);
    exit(1);
    }
  char* filename = argv[1];
  read_file(filename);

  // cwe-276
  // Vulnerable code: Incorrect default permissions
    FILE *file = fopen("sensitive_data.txt", "w"); // Create or overwrite the file
    if (file != NULL) {
        // Write some sensitive data to the file
        fprintf(file, "This is sensitive data that should not be accessible to all users.");
        fclose(file);
        printf("File created successfully.\n");
    } else {
        printf("Error: Unable to create the file.\n");
    }

    // cwe-287  -> C++ only


    // cwe-416
    int* ptr = (int*) malloc(sizeof(int)); // allocate memory
    *ptr = 42; // set the value of the memory

    free(ptr); // free the memory

    // use the pointer after the memory has been freed
    int result = *ptr; // This is a use after free vulnerability

    // cwe-476
    if (argc < 2) {
    printf("Usage: %s <input>\n", argv[0]);
    return 1;
    }

    printf("Running vulnerable function...\n");
    vulnerable_function2(argv[1]);

    // cwe-502 -> JAVA, ruby and php only

    // cwe-78
    char filename2[100];
    FILE *fp;
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }
    strcpy(filename2, argv[1]);
    fp = fopen(filename2, "r");
    if (fp == NULL) {
        printf("Unable to open file: %s\n", filename2);
        return 1;
    }
    // read and process file contents
    fclose(fp);

    // cwe-787 -> only C++ and JAVA

    // cwe-79 -> only .NET, Python

    // cwe-798
    char username2[] = "admin";
    char password[] = "password123";

    authenticate(username2, password);

    // cwe-862 -> only Java, PHP, C++

    // cwe-89 -> only PHP, .NET, JAVA

    // cwe-91 -> only Java, Python

    // cwe-306 -> Java, Python, .NET

    // cwe-352 -> Java, Python, .NET, Nodejs

    // cwe-434 -> Java, Python, .NET

    // cwe-476
    if (argc < 2) {
    printf("Usage: %s <input>\n", argv[0]);
    return 1;
    }

    printf("Running vulnerable function...\n");
    vulnerable_function(argv[1]);

    return 0;
}
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// CWE-119
void copyData(const char* input) {
    char buffer[10];
    strcpy(buffer, input); // Vulnerable line - no bounds checking on input size
    printf("Buffer contents: %s\n", buffer);
}

// CWE-125
void vulnerable_function(char *input_string) {
    char buffer[10];
    strcpy(buffer, input_string); // potential out-of-bounds read vulnerability
    printf("%s\n", buffer);
}

// CWE-20
void display_user(char *username) {
    char sql_query[1000];
    sprintf(sql_query, "SELECT * FROM users WHERE username='%s'", username);
    // Execute the SQL query and display the result
}

// CWE-22
void read_file(char* filename) {
  char path[100];
  snprintf(path, sizeof(path), "/home/user/data/%s", filename);
  FILE* file = fopen(path, "r");
  if (file) {
    printf("File contents:\n");
    char buffer[1024];
    while (fgets(buffer, sizeof(buffer), file)) {
      printf("%s", buffer);
    }
    fclose(file);
  } else {
    printf("File not found.\n");
  }
}

// cwe-476
void vulnerable_function2(char* input) {
  char buffer[100];
  strcpy(buffer, input);

  printf("Copying %d bytes to buffer...\n", strlen(input));

  if (strlen(input) > 50) {
    printf("Input too long!\n");
    return;
  }

  char command[100];
  sprintf(command, "echo %s", buffer);
  system(command);

  char* ptr = NULL;
  *ptr = 'a'; // dereferencing a NULL pointer

  int i;
  for (i = 0; i < strlen(buffer); i++) {
    buffer[i] += 10; // data corruption
  }

  printf("Modified buffer: %s\n", buffer);
}

// cwe-798
void authenticate(char* username, char* password) {
    char validUsername[] = "admin";
    char validPassword[] = "password123";

    if (strcmp(username, validUsername) == 0 && strcmp(password, validPassword) == 0) {
        printf("Authentication successful.\n");
    } else {
        printf("Authentication failed.\n");
    }
}

// cwe-476
void vulnerable_function3(char* input) {
  char buffer[100];
  strcpy(buffer, input);

  printf("Copying %d bytes to buffer...\n", strlen(input));

  if (strlen(input) > 50) {
    printf("Input too long!\n");
    return;
  }

  char command[100];
  sprintf(command, "echo %s", buffer);
  system(command);

  char* ptr = NULL;
  *ptr = 'a'; // dereferencing a NULL pointer

  int i;
  for (i = 0; i < strlen(buffer); i++) {
    buffer[i] += 10; // data corruption
  }

  printf("Modified buffer: %s\n", buffer);
}


int main(int argc, char* argv[]) {
    // CWE-119
    char userInput[20];
    printf("Enter your input: ");
    scanf("%s", userInput);
    copyData(userInput);

    // CWE-125
    char input[20] = "This is a test";
    vulnerable_function(input);

    // CWE-190
    int arr[10];
    int index = 0;
    int input2 = 0;

    // read integers from user input and store them in the array
    while (scanf("%d", &input2) == 1) {
        arr[index] = input2;
        index++;
    }

    // process the array
    int sum = 0;
    for (int i = 0; i <= index; i++) {
        sum += arr[i];
    }

    printf("The sum of the numbers is: %d\n", sum);

    // CWE-20
    char username[100];
    printf("Enter username: ");
    scanf("%s", username);
    display_user(username);

    // CWE-22
    if (argc < 2) {
    printf("Usage: %s filename\n", argv[0]);
    exit(1);
    }
  char* filename = argv[1];
  read_file(filename);

  // cwe-276
  // Vulnerable code: Incorrect default permissions
    FILE *file = fopen("sensitive_data.txt", "w"); // Create or overwrite the file
    if (file != NULL) {
        // Write some sensitive data to the file
        fprintf(file, "This is sensitive data that should not be accessible to all users.");
        fclose(file);
        printf("File created successfully.\n");
    } else {
        printf("Error: Unable to create the file.\n");
    }

    // cwe-287  -> C++ only


    // cwe-416
    int* ptr = (int*) malloc(sizeof(int)); // allocate memory
    *ptr = 42; // set the value of the memory

    free(ptr); // free the memory

    // use the pointer after the memory has been freed
    int result = *ptr; // This is a use after free vulnerability

    // cwe-476
    if (argc < 2) {
    printf("Usage: %s <input>\n", argv[0]);
    return 1;
    }

    printf("Running vulnerable function...\n");
    vulnerable_function2(argv[1]);

    // cwe-502 -> JAVA, ruby and php only

    // cwe-78
    char filename2[100];
    FILE *fp;
    if (argc != 2) {
        printf("Usage: %s <filename>\n", argv[0]);
        return 1;
    }
    strcpy(filename2, argv[1]);
    fp = fopen(filename2, "r");
    if (fp == NULL) {
        printf("Unable to open file: %s\n", filename2);
        return 1;
    }
    // read and process file contents
    fclose(fp);

    // cwe-787 -> only C++ and JAVA

    // cwe-79 -> only .NET, Python

    // cwe-798
    char username2[] = "admin";
    char password[] = "password123";

    authenticate(username2, password);

    // cwe-862 -> only Java, PHP, C++

    // cwe-89 -> only PHP, .NET, JAVA

    // cwe-91 -> only Java, Python

    // cwe-306 -> Java, Python, .NET

    // cwe-352 -> Java, Python, .NET, Nodejs

    // cwe-434 -> Java, Python, .NET

    // cwe-476
    if (argc < 2) {
    printf("Usage: %s <input>\n", argv[0]);
    return 1;
    }

    printf("Running vulnerable function...\n");
    vulnerable_function(argv[1]);

    return 0;
}
