#	Makefile	for	C	project

#	Compiler
CC:=gcc

#Compilerflags
CFLAGS:=-Wall -Wextra

#	Source	file
SRC:=main.c

#	Output	executable
TARGET:=main

all:$(TARGET)

$(TARGET):$(SRC)
	$(CC) $(CFLAGS)	-o$(TARGET) $(SRC)

clean:
	rm -f $(TARGET)